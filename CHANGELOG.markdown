# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased][]

## [4.0.0][] - 2017-07-15
### Changed
* Update style

## [3][] - 2010-11-28
### Added
* Add **changelog** field.

## 2 - 2010-10-20
### Changed
* Remove deprecated attribute **EXCLUDE_MATH**.
* Add a round bracket as **OPERATOR**.

## 1 - 2008-11-19
First public release.

[Unreleased]: https://bitbucket.org/ur6lad/jedit-pkgbuild/branches/compare/tip%0D4.0.0 "Unreleased"
[4.0.0]: https://bitbucket.org/ur6lad/jedit-pkgbuild/branches/compare/4.0.0%0D3 "v4.0.0"
[3]: https://bitbucket.org/ur6lad/jedit-pkgbuild/branches/compare/3%0D2 "v3"