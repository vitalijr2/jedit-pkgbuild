# The jEdit edit mode file for Arch Linux PKGBUILD.

![AUR version](https://img.shields.io/aur/version/jedit-pkgbuild)
![AUR last modified](https://img.shields.io/aur/last-modified/jedit-pkgbuild)
![AUR license](https://img.shields.io/aur/license/jedit-pkgbuild)

## How to install

### Linux

If you use Arch Linux you can install the AUR package [jedit-pkgbuild][package].

For other Linux distributives and BSD:

* Copy **pkgbuild.xml** to the folder **$HOME/.jedit/modes**.
* Add to the XML file **$HOME/.jedit/modes/catalog** before `</MODES>`:

```xml
    <MODE NAME="PKGBUILD" FILE="pkgbuild.xml" FILE_NAME_GLOB="PKGBUILD"/>
```

### Windows

* Copy **pkgbuild.xml** to the folder **%HOMEDRIVE%%HOMEPATH%/.jedit/modes**.
* Add to the XML file **%HOMEDRIVE%%HOMEPATH%/.jedit/modes/catalog** before `</MODES>`:

```xml
    <MODE NAME="PKGBUILD" FILE="pkgbuild.xml" FILE_NAME_GLOB="PKGBUILD"/>
```

## Changelog

See [CHANGELOG][]

## Acknowledgments

My project is based on [pkgbuild.xml][] of [Dmitry Stropaloff aka h8][h8]

[package]: https://aur.archlinux.org/packages/jedit-pkgbuild/ "AUR package"
[CHANGELOG]: CHANGELOG.markdown
[pkgbuild.xml]: http://www.linux.org.ru/gallery/screenshots/1783210
[h8]: http://h8-self.livejournal.com/ "__h8__(self):"
